;;; -*- Mode: LISP; Package: (SCREAMER :USE CL :COLON-MODE :EXTERNAL); Base: 10; Syntax: Ansi-common-lisp -*-

(in-package :screamer)

(declaim (declaration magic))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (screamer::declare-nondeterministic 'an-integer-above))

;; an-integer-above - это из того, что мы стёрли.
(cl:defun an-integer-above (low)
  "Generator yielding integers starting from LOW and continuing sequentially
in increasing direction."
  (declare (ignore low))
  (screamer-error
   "AN-INTEGER-ABOVE is a nondeterministic function. As such, it must be~%~
   called only from a nondeterministic context."))

(cl:defun an-integer-above-nondeterministic (continuation low)
  (let ((low (ceiling (screamer::value-of low))))
    (screamer::choice-point-external
     (let ((i low))
       (loop (screamer::choice-point-internal (funcall continuation i))
         (incf i))))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (declare-nondeterministic 'a-char-in-the-stream))

;; а это уже моё производство
(cl:defun a-char-in-the-stream (stream)
  "Generator yields all chars from the stream and then fails"
  (declare (ignore stream))
  (screamer-error
   "A-CHAR-IN-THE-STREAM is a nondeterministic function. As such, it must be~%~
   called only from a nondeterministic context."))

(cl:defun a-char-in-the-stream-nondeterministic (continuation stream)
          (choice-point-external
           (loop
             (let ((char (read-char stream nil nil)))
               (unless char (return))
               (choice-point-internal (funcall continuation char)))))
          (fail) ; ??? Этого у них не было
          )
