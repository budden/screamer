;;; -*- Mode: LISP; Package: (PRIMORDIAL :USE CL :COLON-MODE :EXTERNAL); Base: 10; Syntax: Ansi-common-lisp -*-

(eval-when (:compile-toplevel :load-toplevel)
  (require :iterate))

(in-package :screamer-user)

(screamer:define-screamer-package :primordial (:use :iterate :cl :screamer))

(in-package :primordial)

(defun equal-set? (x y)
 (and (subsetp x y :test #'equal) (subsetp y x :test #'equal)))

(defun a-bit () (either 0 1))

;;; note: DOLIST and DOTIMES work nondeterministically because PUSH doesn't
;;;       destructively modify the list that is being collected so each list
;;;       returned as a nondeterministic value is available after backtracking.
(defun test3-internal (n)
 (local
  (let (collection)
   (dotimes (i n) (push (either 0 1) collection))
   collection)))

(defun test3 ()
 (equal-set? (all-values (test3-internal 2)) '((0 0) (1 0) (0 1) (1 1))))

(defun test4-internal (n)
 (local (let (collection)
	 (dotimes (i n) (push (a-bit) collection))
	 collection)))

(defun test4 ()
 (equal-set? (all-values (test3-internal 2)) '((0 0) (1 0) (0 1) (1 1))))

(defun test5-internal (list)
 (local (let (collection)
	 (dolist (e list)
           (declare (ignore e))
           (push (either 0 1) collection))
	 collection)))

(defun test5 ()
 (equal-set? (all-values (test3-internal 2)) '((0 0) (1 0) (0 1) (1 1))))

(defun test6-internal (list)
 (local (let (collection)
	 (dolist (e list)
           (declare (ignore e))
           (push (a-bit) collection))
	 collection)))

(defun test6 ()
 (equal-set? (all-values (test3-internal 2)) '((0 0) (1 0) (0 1) (1 1))))

;;; Problems with LOOP:
;;;  2. Results disappear upon ALL-VALUES internal backtracking.

#+comment
(defun test7-internal (n) (local (loop repeat n collect (either 0 1))))

#+comment
(defun test7 ()
 (equal-set? (all-values (test7-internal 2)) '((1 1) (0 1) (1 0) (0 0))))

#+comment
(defun test8-internal (n) (local (loop repeat n collect (a-bit))))

#+comment
(defun test8 ()
 (equal-set? (all-values (test8-internal 2)) '((1 1) (0 1) (1 0) (0 0))))

;;; Problems with ITERATE:
;;;  1. Beta conversion of (let ((#:foo nil)) (setf foo 'bar)) is unsound.
;;;  2. Results disappear upon ALL-VALUES internal backtracking.

#+comment
(defun test9-internal (n)
 (local (iterate (repeat n) (collect (either 0 1)))))

#+comment
(defun test9 ()
 (equal-set? (all-values (test9-internal 2)) '((1 1) (0 1) (1 0) (0 0))))

#+comment
(defun test10-internal (n) (local (iterate (repeat n) (collect (a-bit)))))

#+comment
(defun test10 ()
 (equal-set? (all-values (test10-internal 2)) '((1 1) (0 1) (1 0) (0 0))))

#+comment
(defun bar (n)
 (local (LET* ((COUNT789)
	       (RESULT788)
	       (END-POINTER790)
	       (TEMP791))
	      (BLOCK NIL
		     (TAGBODY
		      (SETQ COUNT789 N)
		      LOOP-TOP-NIL
		      (IF (<= COUNT789 0) (GO LOOP-END-NIL))
		      (PROGN
		       (SETQ TEMP791 (LIST (A-BIT)))
		       (IF TEMP791
			   (SETQ END-POINTER790
				 (IF RESULT788
				     (SETF (CDR END-POINTER790) TEMP791)
				     (SETQ RESULT788 TEMP791))))
		       RESULT788)
		      LOOP-STEP-NIL
		      (SETQ COUNT789 (1- COUNT789))
		      (GO LOOP-TOP-NIL)
		      LOOP-END-NIL)
		     RESULT788))))

